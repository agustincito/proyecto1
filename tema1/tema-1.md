## Herramientas del Markdown para textos
Listemos algunas de las herramientas que podemos utilizar en este editor de texto:
- Bold (**Negrita**): Nos sirve para **resaltar una palabra o frase** en negrita, lo que tenemos que hacer es escribir dos asteriscos al principio y final de la palabra o texto a editar.
- Italic (*Inclinaci�n*): Para *resaltar* una palabra y escribirla *inclinada* lo que tenemos que hacer es escribirla entre un asterisco por cada lado.
- Italic and Bold (***Inclinado y en negrita***): Se puede hacer una ***combinaci�n de los dos comandos anteriores *** a un texto, escribiendo al texto en cuesti�n entre tres asteriscos por cada lado. 
- Strikethrough (~~Tachado~~): Podemos ~~tachar una frase o palabra~~ usando esta herramienta, escribiendo dos virgulillas al principio y final de la palabra o frase.
- Block quote (Cita): Si queremos a�adir a nuestro escrito una frase textual usamos este comando, por ejemplo:

> l'essentiel est invisible pour les yeux  - Antoine de Saint-Exup�ry.

- Words first letters convert to uppercase (La primera letra de la palabra en may�scula): Si la palabra seleccionada esta escrita toda en min�scula, vuelve a la primera letra en May�scula.
- Selection text convert to uppercase (El texto seleccionado todo en may�sculas): Similar a la funcion anterior, permite que toda una palabra se escriba en MAY�SCULAS.
- Selection text convert to lowercase (El texto seleccionado todo en min�sculas): Realiza la acci�n contraria al comando anterior, escribiendo una palabra que est� en MAY�SCULAS en min�sculas.