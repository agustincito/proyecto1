Trabajo Pr�ctico 2

# �Por qu� nuestro reloj nos miente?
Cuando nos preguntan que hora es, lo �nico que hacemos es fijarnos la posici�n de las agujas del reloj y ver que n�mero marcan, o simplemente fijarnos en nuestro celular. Pero, �La hora que marca nuestro reloj coincide con el astro rey que nos determina cuando es de d�a y de noche? Bueno, tratar� de explicar por que **NO** es as�.

## Los husos horarios
Primero definamos lo que es un huso horario (en este caso es con *h* y no tiene nada que ver con el verbo usar). En geograf�a, un **huso horario** es cada una de las 24 regiones de tiempo cronom�trico en el que se divide nuestro planeta. Cada regi�n se denomina "zona o franja horaria", y tiene una hora asignada que rige para todos aquellos que se encuentren cerca de ese meridiano geogr�fico. El **meridiano de Greenwich** se toma como referencia para establecer el resto de las franjas horarias.

Como divide al mundo en 24 "columnas" equitativas, podemos calcular f�cilmente la hora de dos pa�ses que se encuentran muy distantes entre s�. Lo �nico que hay que hacer para calcular la diferencia horaria es contar cuantos husos horarios debemos "atravesar" para llegar a determinado lugar. Si se avanza hacia la *derecha* del mapamundi (Este) debemos sumar una hora por cada regi�n horaria pasada, y si se avanza hacia la *izquierda* (Oeste) debemos hacer lo contrario, es decir, restar una hora por cada regi�n horaria pasada.

## El caso argentino
Bien, Argentina utiliza el huso horario de tres horas menos a la del meridiano de Greenwich hacia el oeste (UTC -3) en todo su territorio, pero si agarramos un mapa, podemos ver que se encuentra ubicada (la mayor parte de su superficie) dentro de la zona horaria UTC -4. Quiere decir que estamos adelantados una hora con respecto al sol.

�C�mo nos damos cuenta de esto? Simplemente hay que observar cuando se produce el mediod�a solar, es decir, a que hora el sol se encuentra justo sobre nuestras cabezas. Generalmente en Buenos Aires se da a las 13 hs, mientras que en zonas m�s occidentales, como en Bariloche, esto puede ocurrir incluso pasada las 14 hs.

Veamos la hora en que se produce la salida y puesta de sol, y el mediodia solar en distintas ciudades argentinas el 20 de enero de 2020

| Ciudad | Hora del mediod�a solar | Salida del Sol | Puesta del Sol |
| :---: | :---: | :---: | :---: |
| Posadas | 12:54 | 6:06 | 19:42 |
| Buenos Aires | 13:04 | 6:01 | 20:07 |
| C�rdoba | 13:27 | 6:31 | 20:23 |
| San Salvador de Jujuy | 13:32 | 6:50 | 20:14 |
| Mendoza |  13:46 | 6:46 | 20:45 |
| Bariloche | 13:52 | 6:24 | 21:20 |
| Ushuaia | 13:40 | 5:12 | 22:07 |

Podemos ver que en ninguna ciudad el mediod�a solar se encuentra cercano a las 12:00, y que en las ciudades m�s australes el sol se pone despu�s de las 21:00.

### �C�mo se lleg� a esto?
Cada pa�s ejerce su soberan�a y elige que huso horario se utilizar� dentro de sus fronteras. Hasta el siglo XIX, en nuestro pa�s cada pueblo utilizaba su propia hora local, teniendo como referencia el sol, el servicio hidronaval local o el observatorio astron�mico. Con la expansi�n del tren hacia el interior, y el boom que tuvo el telegrafo a finales de siglo, nuestro pa�s tuvo que coordinarse para establecer un �nico horario unificado, sino las personas no sabr�an a que hora deb�an estar en la estaci�n para no perder el tren de las diez.
En 1920, la Argentina se adhiri� a la Convenci�n Internacional del Huso Horario, adoptando la franja horaria UTC -4, ya que la mayoria del territorio se ubica all�. En ese momento se sab�a que la diferencia entre el este y oeste de nuestro pa�s es de m�s de una hora, pero como en ese tiempo el oeste se encontraba mayormente *deshabitado* , decidieron utilizar una �nica hora oficial.
A partir de 1930 el gobierno decide utilizar el famoso "horario de verano". El horario de verano implica en adelantar el reloj una hora para aprovechar mejor la luz de sol en la �poca estival y as� provocar un ahorro energ�tico significativo. Hasta los a�os 70, hubieron periodos donde se aplicaba esta alternancia horaria (de 1930 a 1941, en 1943, 1946 y de 1963 a 1969). Lo que m�s llamo la atenci�n es que en los a�os en los que no se aplicaba el cambio de horario, el pa�s usaba todo el a�o el horario de verano (normalmente se prefiere usar el horario de invierno en vez del de verano).
En 1974, con el horario de verano ya implementado, se decide adelantar otra vez una hora los relojes, haciendo que el pa�s quede dentro del UTC -2 (geograficamente hablando, el pa�s se encontrar�a en medio del oc�ano Atl�ntico). Se siguieron utilizando los husos UTC -3 y -2 para establecer los horarios de invierno y verano, respectivamente, hasta 1993, cuando se dejo de usar el horario de verano y se decidio utilizar todo el a�o el UTC -3.
En 2007, el gobierno decide otra vez implementar el horario de verano en todo el territorio, adelantando la hora para ajustarse al huso horario UTC -2 durante el verano 2007-2008.
A finales de 2008, la pol�mica sobre la efectividad del ahorro energ�tico al adelantar la hora se acrecent� m�s, provocando que el pa�s quede dividido en dos horas diferentes durante el peri�do estival 2008-2009. Las provincias m�s orientales adelantaron sus relojes una hora, mientras que el resto qued� utilizando el UTC -3 durante todo el a�o.
Otro "problema" que surgi� en 2009, es que cuando las provincias que adelantaron sus relojes volvieron al horario de invierno, San Luis tambi�n atraso su hora (hay que tener en cuenta que esta provincia no habia adoptado el horario de verano el a�o anterior), dej�ndola con una hora distinta con respecto a sus provincias vecinas. Ese mismo a�o, solo Misiones y Buenos Aires aceptaron adelantar una hora sus relojes durante el verano 2009-2010, entonces el gobierno decidi� descartar su implementaci�n hasta el d�a de hoy. San Luis utiliz� su horario especial durante 2010, pero luego adopt� el horario est�ndar que se usa en toda la naci�n.


![](https://lh3.googleusercontent.com/proxy/EJU_v5oJsvPKpQ5AoLlLgfvSBpSOPioibrvPTfM-bHuL6p9LCyOuVPTLk7ppGuyAfsxQ9IC9miaROTZeRLCjxqKXnxT1e4c33241mpwDKVjM)

En el mapa podemos ver que el pa�s se encuentra entre las franjas horarias de UTC -5 y -4

## Consecuencias de un horario desfasado
Un huso horario incorrecto puede traer consigo algunos problemas que pueden afectar tanto a nuestro cuerpo como a nuestras costumbres.
El ritmo circadiano, presente en los seres vivos, es aquel que regula las actividades biol�gicas en base a periodos de tiempo, en pocas palabras, es aquello que nos mantiene despiertos de d�a y dormidos por la noche. Este ritmo regula los horarios de vigilia, de alimentaci�n y metab�licos, entre muchos otros. 
El problema empieza cuando existe una diferencia notable entre el ritmo circadiano (que es regulado por la luz solar) y el ritmo de la vida cotidiana en t�rminos cronol�gicos. Nuestro d�a a d�a esta marcado por el reloj que nos marca la hora en nuestros hogares, nos dice cuando levantarnos, almorzar, entrenar, estudiar, dormir y muchas otras m�s actividades. Si este reloj que nos dice la hora esta desfasado con la hora biol�gica del cuerpo, nuestro rendimiento y salud se ven directamente afectados por esta diferencia.
Veamos como impacta esto en nuestro pa�s, teniendo en cuenta que las provincias del Este estan adelantadas una hora respecto al mediod�a solar y las del Oeste dos horas: iniciamos generalmente el d�a a las 7, cuando nos levantamos y preparamos para ir a trabajar o estudiar. Nos cuesta levantarnos y somos mucho m�s lentos durante las primeras horas de vigilia, ya que para nuestro cuerpo reci�n son las 5 o 6 de la madrugada. Otro contratiempo que hay es cuando nos vamos a dormir, nos acostamos a las 12, pero a veces damos vueltas en la cama durante horas sin conciliar el sue�o. Esto pasa porque la actividad cerebral todav�a se encuentra activa, por lo tanto reci�n el cuerpo *estar�a* preparado para dormir reci�n entre la 1 y las 2 de la madrugada. Como al d�a siguiente tenemos que despertarnos temprano y nos dormimos tarde, es normal que nos sintamos fatigados durante el d�a al no descansar adecuadamente. As� se mantiene este c�rculo vicioso de un descanso no satisfactorio. Se sabe que no descansar bien trae consigo varios problemas para la salud, pero no desarrollar� sobre este tema en esta ocasi�n.
Aunque la hora del pa�s es la que nos establece cuando hacer cada cosa, nuestras costumbres se han desplazado una o dos horas hacia delante para que la diferencia entre el ritmo social y biol�gico no sea tan brusca. Generalmente almorzamos entre las 13 y 14 horas, pero para nuestro cuerpo es reci�n el mediod�a, horario donde metab�licamente el sistema digestivo se encuentra m�s activo. Los negocios abren y cierran m�s tarde, y aunque tambi�n pensemos que nos estamos volviendo trasnochadores al acostarnos despu�s de la 1 de la madrugada, biologicamente hablando, nuestro cuerpo lo hace a la hora indicada.
Este problema est� mucho m�s presente en la regi�n patag�nica en el invierno y verano. La gente se despierta sin luz solar durante los meses de invierno, teniendo un impacto an�mico, y se duerme cuando todav�a esta presente la luz solar en el verano (hay que tener en cuenta que en el Sur el sol se pone reci�n entre las 21 y 22 hs). Todo esto se agrava con un horario de verano de UTC -2, en enero de 2008 en Tierra del Fuego el sol se ocultaba por detr�s del horizonte reci�n **�A las 11 de la noche!**
Otro inconveniente que existe en verano es la incidencia de los rayos solares y nuestra piel. Los especialistas de la piel recomiendan no tomar sol entre las 12 y 17, ya que los rayos UV son mucho m�s da�inos y peligrosos. Obviamente el horario toma en cuenta el movimiento del sol en el cielo. Pero como no se suele especificar que en Argentina los horarios m�s peligrosos para tomar sol se atrasan entre una a dos horas del horario estipulado como perjudicial, la gente estar� expuesta a rayos potencialmente da�inos pensando que esta fuera del periodo de exposici�n de m�s riesgo.

## Posible Soluci�n
La mejor soluci�n, teniendo en cuenta criterios cient�ficos, astron�micos y geogr�ficos, es dividir al pa�s en dos horas distintas, usando los husos horarios UTC -5 para la Patagonia y Cuyo, y UTC -4 para el resto. Mucha gente argumentar� que esto traer� varios problemas, al no tener una hora unificada, es f�cil pensar que ser� muy dif�cil coordinar actividades en el pa�s debido a la diferencia horaria. Esto es totalmente falso, ya que en pa�ses como M�xico, Rusia o Estados Unidos, que son longitudinalmente m�s extensos, utilizan dos o m�s horas distintas sin ning�n problema, es solo cuesti�n de acostumbrarse y adaptarse.
Otra opci�n menos separatista es que en todo el pa�s se utilice como horario est�ndar el UTC -4 y no UTC-3 como se utiliza actualmente. Esto har�a que la diferencia horaria entre el sol y la sociedad no sea tan distinta.
Por �ltimo, el tema del horario de verano para el ahorro energ�tico ya no es tan conveniente. En un pasado, este m�todo para ahorrar electricidad relativamente funcionaba, ya que las personas aprovechaban mejor la luz solar y no gastaban tanta energ�a durante la noche. Hoy en d�a, con la tecnolog�a actual que maneja la mayor parte de nuestra poblaci�n, adelantar una hora nuestros relojes tendr�a muy poco impacto o directamente nulo, ya que hoy la gente utiliza elementos que consumen electricidad tanto de d�a como de noche.

Para finalizar, como la cuesti�n de nuestro huso horario no es un tema que genere discuciones en la poblaci�n, es muy probable que sigamos con nuestra ***errada forma de medir el tiempo*** por varios a�os m�s.




