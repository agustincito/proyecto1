## Herramientas del Markdown para textos

Enumeremos algunos de las herramientas que podemos utilizar en este editor de texto

- Bold (**Negrita**): Nos sirve para **resaltar una palabra o frase** en negrita, lo que tenemos que hacer es escribir dos asteriscos al principio y final de la palabra o texto a editar.

- Strikethrough (~~Tachado~~): Podemos ~~tachar una frase o palabra~~ usando esta herramienta, escribiendo dos virgulillas al principio y final de la palabra o frase.

- Italic (*Inclinaci�n*): Para *resaltar* una palabra y escribirla *inclinada* lo que tenemos que hacer es escribirla entre un asterisco por lado.

- Block quote (Cita): Si queremos a�adir a nuestro escrito una frase textual usamos este comando:

> l'essentiel est invisible pour les yeux  - Antoine de Saint-Exup�ry.

- Words first letters convert to uppercase (La primera letra de la palabra en may�scula): Si la palabra seleccionada esta escrita toda en min�scula, vuelve a la primera letra en May�scula.

- Selection text convert to uppercase (El texto seleccionado todo en may�sculas): Similar a la funcion anterior, permite que toda una palabra se escriba en MAY�SCULAS.

- Selection text convert to lowercase (El texto seleccionado todo en min�sculas): Realiza la acci�n contraria al comando anterior, escribiendo una palabra que est� en MAY�SCULAS en min�sculas.