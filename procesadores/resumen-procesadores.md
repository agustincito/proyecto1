# Organizaci�n de los Sistemas de una Computadora
## Procesadores
La organizaci�n de una computadora esta constituida principalmente por los siguientes componentes:
La **CPU** (unidad central de procesamiento, en ingl�s conocida como *Central Processing Unit*) es el "cerebro" de la computadora. Esta ejecuta programas almacenados en la memoria principal buscando instrucciones para luego examinarlas y ejecutarlas.
Los componentes estan conectados a trav�s de un **bus**, que es un conjunto de alambres paralelos para transmitir direcciones, datos y se�ales de control. Estos pueden ser externos, cuando se conectan a la memoria y a los **dispositivos de entrada/salida** o internos.
La CPU tambi�n posee una peque�a y r�pida memoria, que se encuentra conformada principalmente por **registros**, cada uno con distintos tama�os y funciones, como:
- **Contador de Programa** (PC, *Program Counter*): Es el registro m�s importante, ya que apunta a la siguiente instrucciones que debe buscarse para ejecutarse.
- **Registro de Instrucciones** (IR, *Instructi�n Register*): Contiene la instrucci�n que se est� ejecutando.
La mayoria de las computadoras tienen muchos m�s registros, con distintos prop�sitos y fines.
En una t�pica maquina de **von Neumann** podemos encontrar una parte conocida como **camino de datos**, que consiste en los registros, la **ALU** (unidad aritm�tico-l�gica, *Arithmetic Logic Unit*) y **buses** que conectan los componentes. Los registros alimentan dos registros de la entrada ALU. Estos registros tienen las entradas de la ALU mientras �sta est� calculando.
La **ALU** suma, resta y realiza otras operaciones b�sicas con las entradas, produce un resultado y luego lo deja registrado (lo guarda) en la memoria.
Las instrucciones que realiza se pueden dividir en dos tipos; las instrucciones **registro-memoria** y las **registro-registro**. Las del primer tipo permite buscar palabras de la memoria a los registros o almacenar el contenido de un registro en una memoria, en cambio las otras buscan dos operandos de los registros, los coloca en los registro de entrada de la ALU, realiza la operaci�n sencilla y pone el resultado en uno de los registros.
## Memoria Primaria o Principal
La **memoria** es la parte de la computadora donde se almacenan los programas y datos. Algunos especialistas de la computaci�n utilizan t�rminos como **almac�n** o **almacenamiento** (en ingl�s denominados *store* y *storage* respectivamente) en lugar de simplemente *memoria*.
Las memorias consisten principalmente en varias celdas (o localidades), cada una de las cuales puede almacenar un elemento de informaci�n. Cada una de estas celdas tienen un n�mero y direcci�n �nicos, con los cuales los programas pueden referirse a ellas. Todas las celdas pertenecientes a una misma memoria contienen el mismo n�mero de bits.
Algunos ejemplos de memoria primaria son la **RAM** (*random access memory*) y la **ROM** (*read only memory*)
## Perif�ricos de Entrada/Salida
Se llaman perif�ricos a los aparatos y dispositivos auxiliares independientes que estan conectados a la unidad central de procesamiento de la computadora. Se dividen en dos tipos, los de entrada y los de salida:
- **Perif�ricos de Entrada** (Input device): Son los que permiten introducir datos externos a la computadora para su posterior tratamiento por parte de la CPU.
- **Perif�ricos de Salida** (Output device): Son los que reciben la informaci�n procesada por la CPU y la reproducen, para que sea perceptible por el usuario.
## Buses de datos
En ingl�s la palabra *bus* significa transporte. Un bus de datos puede conectar l�gicamente varios perif�ricos sobre un mismo conjunto de cables. Est� relacionado con la idea de las transferencias internas de datos que se dan en un sistema computacional en funcionamiento.